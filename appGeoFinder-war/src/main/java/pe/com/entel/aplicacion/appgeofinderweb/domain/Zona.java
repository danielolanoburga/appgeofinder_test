
package pe.com.entel.aplicacion.appgeofinderweb.domain;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by DOLANO - PRY-1443 on 12/05/2019.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "tipo"
})
public class Zona {

    @JsonProperty("tipo")
    private String tipo;

    @JsonIgnore
    private String valor;

    @JsonIgnore
    private Integer hierarchyId;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("tipo")
    public String getTipo() {
        return tipo;
    }

    @JsonProperty("tipo")
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /*
    @JsonProperty("valor")
    public List<Valor> getValor() {
        return valor;
    }

    @JsonProperty("valor")
    public void setValor(List<Valor> valor) {
        this.valor = valor;
    }
    */

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonIgnore
    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    @JsonIgnore
    public Integer getHierarchyId() {
        return hierarchyId;
    }

    public void setHierarchyId(Integer hierarchyId) {
        this.hierarchyId = hierarchyId;
    }

    @Override
    public String toString() {
        return "Zona{" +
                "tipo='" + tipo + '\'' +
                ", valor='" + valor + '\'' +
                ", hierarchyId=" + hierarchyId +
                '}';
    }
}
