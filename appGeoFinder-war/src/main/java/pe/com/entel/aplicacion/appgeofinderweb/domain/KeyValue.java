package pe.com.entel.aplicacion.appgeofinderweb.domain;

import java.io.Serializable;

/**
 * Created by DOLANO - PRY-1443 on 13/05/2019.
 */
public class KeyValue implements Serializable {

    private String key;
    private String value;

    public KeyValue() {
    }

    public KeyValue(String value, String key) {
        this.value = value;
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
