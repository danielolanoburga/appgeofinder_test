package pe.com.entel.aplicacion.appgeofinderweb.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;


public class  Common {
	
	private  static final Logger logger = Logger.getLogger(Common.class);
	
	private static ResourceBundle resource= null;
	
	public static Integer convertToInt(String var){
		return new Common().convertToInteger(var);
	}
	
	public Integer convertToInteger(String variable){
		logger.debug("[convertToInteger] variable="+variable+" - "+variable.isEmpty());
		if(!variable.isEmpty() && isDigit(variable))
			return Integer.parseInt(variable);
		return null;
	}
	
	public static boolean isDate(String variable){
		logger.debug("[isDate] variable="+variable);
		if(variable.matches("^\\d{1,2}/\\d{1,2}/\\d{2,4}$"))
			return true;
		return false;
	}
	
	public boolean isDigit(String variable){
		logger.debug("[isDigit] variable="+variable);
		if(!variable.isEmpty() && variable.matches("\\d*"))
			return true;
		return false;
	}
	public boolean isDigitWithCharacter(String variable){
		logger.debug("[isDigitWithCharacter] variable="+variable);
		if(variable.matches("[A-ZÑ0-9]*"))
			return true;
		return false;
	}
	
	public boolean isCharacterOnlyLetter(String variable){
		logger.debug("[isCharacterOnlyLetter] variable="+variable);
		if(variable.matches("[A-ZÑ]*"))
			return true;
		return false;
	}
	
	public static String convertDateToString(Date fecha) {
		logger.debug("[convertDateToString] fecha="+fecha);
		try {
			SimpleDateFormat dFormato = new SimpleDateFormat("dd/MM/yyyy");
			return dFormato.format(fecha);
		} catch (Exception e) {
			return null;
		}
	}
	
	public static Date convertStringtoDate(String stringDate){
		logger.debug("[convertStringtoDate] isNull="+stringDate);
		if(null == stringDate)
			return null;
		logger.debug("[convertStringtoDate] stringDate="+stringDate+"  - "+stringDate.isEmpty());
		if(stringDate.isEmpty() || stringDate.equals(""))
			return null;
		try{
			if(!isDate(stringDate))
				return null;
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			return dateFormat.parse(stringDate);
		}catch(Exception e){
			return null;			
		}
	}
	
	public boolean isDigitCharacterWithSpace(String variable){
		logger.debug("[isDigitCharacterWithSpace] stringDate="+variable);
		if(variable.matches("[-A-ZÑ0-9_.'\"& ]*"))
			return true;
		return false;
	}
	
	public Calendar stringToCalendar(String strFecha){
		logger.debug("[stringToCalendar] stringDate="+strFecha);
		if(null == strFecha)
			return null;
		if(strFecha.equals("") || strFecha.equalsIgnoreCase("null"))
			return null;		
		if(!isDate(strFecha))
			return null;
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendario = null;
		try {
			calendario=df.getCalendar();
			calendario.setTime(df.parse(strFecha));
		} catch (Exception excepcion) {
			System.out.println("Calendar fechaCanledar  ="+excepcion.getMessage());
			return null;
		}
		return calendario;
	}
	
	public static Long convertStringToLong(String cadena){
		Long numero = null;
		try{
			numero = Long.parseLong(trimNotNull(cadena));
		}catch (Exception e) {
			numero = null;
		}		 
		return numero;
	}
	
	public static String trimNotNull(String cadena){
		if(cadena == null){
	        cadena = "";
	    }else{
	        cadena = cadena.trim();
	    }
	    return cadena;
	}
	
	
	private static Context getInitialContext() throws NamingException {
	    Properties env = new Properties();
	    env.put( Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory" );
	    env.put(Context.PROVIDER_URL," t3://localhost:9004");
	    return new InitialContext( env );
	  }
	
	public static Object obtenerContexto(String jndiRemove, String dirClase){
		Object objeto = null;
		try {
			Context ctx = getInitialContext();
			System.out.println("antes de invocar al getInitialContext");
			
			//Object objeto = ctx.lookup("SEJBSessionPortalRemote-SEJBSessionPortal#com.pe.nextel.portalapp.ejb.SessionPortal");
			objeto = ctx.lookup(jndiRemove+"#"+dirClase);
			
			System.out.println("despues de invocar al getInitialContext");
			
		} catch (NamingException e) {
			System.out.println("Error  NamingException "+e.toString());
			e.printStackTrace();
		}
		return objeto;
	}
	
	

}
