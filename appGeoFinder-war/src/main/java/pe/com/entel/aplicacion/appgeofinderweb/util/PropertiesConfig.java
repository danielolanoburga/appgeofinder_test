package pe.com.entel.aplicacion.appgeofinderweb.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by JTALLEDO on 24/03/2018.
 */
@Component
public class PropertiesConfig {

    @Value( "${wsdl.encrypt.location}" )
    private String wsdlEncryptLocation;

    public String getWsdlEncryptLocation() {
        return wsdlEncryptLocation;
    }

    public void setWsdlEncryptLocation(String wsdlEncryptLocation) {
        this.wsdlEncryptLocation = wsdlEncryptLocation;
    }
}
