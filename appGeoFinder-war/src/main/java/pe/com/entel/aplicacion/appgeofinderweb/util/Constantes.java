package pe.com.entel.aplicacion.appgeofinderweb.util;

public class Constantes {

    public static final String C_PORTLET_EVAL_LOCATION_PARAM_DOC_TYPE = "prmDocType";
    public static final String C_PORTLET_EVAL_LOCATION_PARAM_DOC_NUM = "prmDocNum";
    public static final String C_PORTLET_EVAL_LOCATION_PARAM_BUILDING_ID = "prmBuildingId";
    public static final String C_PORTLET_EVAL_LOCATION_PARAM_USER_LOGIN = "prmUserLogin";
    public static final String C_PORTLET_EVAL_LOCATION_PARAM_APP_ID = "prmAppId";
    public static final String C_PORTLET_EVAL_LOCATION_PARAM_FLAG_EVAL_COVERAGE = "flagEvalCobertura";
    public static final String C_PORTLET_EVAL_LOCATION_PARAM_MESSAGE = "strMessage";
    public static final String C_STATUS_OK = "OK";
    public static final String C_STATUS_ERROR = "ERROR";
    public static final String ENTEL_PROPERTIES = "entel.properties";
    public static final String APP_DIRECTORY = "AppGeoFinderWeb";
    public static final String LOG4J_PROPERTIES = "log4j.properties";
    public static final String C_PORTLET_GEOFINDER_KEY = "prmGeoFinderKey";

    public static final int C_STATUS_GEOFINDER_ZONE = 1;     //tiene Cobertura
    public static final int C_STATUS_GEOFINDER_NO_ZONE = 0;  //Sin Cobertura
    public static final int C_STATUS_GEOFINDER_ERROR = -1;   //Otros Errores
    public static final int C_STATUS_GEOFINDER_INVALID = -2; //Busqueda invalida

    public static final String C_ERROR_MESSAGE_GF_ERROR = "Error en el componente de GeoFinder";
    public static final String C_ERROR_MESSAGE_GF_INVALID = "Se realizo una busqueda invalida";
    public static final String C_ERROR_MESSAGE_BD_ENCRYPT = "Error en el SP de encriptacion";
    public static final String C_ERROR_MESSAGE_OSB_ENCRYPT = "Error en el OSB de encriptacion";
    public static final int C_ON = 1;
    public static final String C_DEFAULT_KEY_GEOFINDER = "shzywi6wgmzcs26fybgswx6wt5y1px11";
    public static final String C_CHANNEL_WS = "CRM";

    //LGONZALES PRY-1140
    public static final String C_SITE_TYPE_COVERAGE = "COB";
    public static final String C_SITE_TYPE_HSZ = "HSZ";
    public static final String C_ERROR_MESSAGE_LOG_SAVE = "Error al guardar HOA_SCOPE_ADDRESS_LOG";
    public static final String C_ERROR_MESSAGE_GET_CODE_BUILDING = "Error al obtener Codigo de Tienda";
    public static final String C_SUCCESS_MESSAGE_LOG_SAVE = "Se guardo exitosamente en HOA_SCOPE_ADDRESS_LOG";
    public static final String C_GEOFINDER_INIT_URL = "/appGeoFinderWeb/initGeoFinder";
    public static final String C_PORTLET_EVAL_LOCATION_PARAM_APP_ORIGIN = "prmAppOrigin";
    public static final String C_PORTLET_EVAL_LOCATION_PARAM_ADDRESS_TYPE = "prmAddressType";
    public static final String C_PORTLET_EVAL_LOCATION_PARAM_INCIDENT_ORIGIN_VALUE= "INC";

    // BEGIN: PRY-1313 - DOLANO
    //public static final String C_SITE_TYPE_COD = "COD";
    //public static final String C_SITE_TYPE_CID = "CID";
    //public static final int C_STATUS_GEOFINDER_ZONE_OUTDOOR_INDOOR = 2;     //tiene Cobertura Outdoor/Indoor
    //public static final int C_STATUS_GEOFINDER_ZONE_OUTDOOR = 3;     //tiene solo Cobertura Outdoor
    //public static final int C_STATUS_GEOFINDER_ZONE_INDOOR = 4;     //tiene solo Cobertura Indoor
    public static final String C_UNDEFINED = "UNDEFINED";     //tiene solo Cobertura Indoor
    public static final String C_PORTLET_EVAL_LOCATION_PARAM_COD_TYPE_PROD = "prmCodTypeProd";
    public static final String C_PORTLET_EVAL_LOCATION_PARAM_PHONE = "prmPhone";
    // END: PRY-1313 - DOLANO

    // BEGIN: PRY-1443 - DOLANO
    public static final String C_TYPE_RES = "RES";
    public static final String C_TYPE_SITE = "SITE";
    // END: PRY-1443 - DOLANO


}
