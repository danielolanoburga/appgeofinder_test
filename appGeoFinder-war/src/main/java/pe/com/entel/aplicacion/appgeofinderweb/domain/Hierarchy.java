package pe.com.entel.aplicacion.appgeofinderweb.domain;

import java.io.Serializable;

/**
 * Created by DOLANO - PRY-1443 on 12/05/2019.
 */
public class Hierarchy implements Serializable {

    private Integer id;
    private Integer nivel;
    private String name;
    private String description;
    private Integer parentId;

    public Hierarchy() {
    }

    public Hierarchy(Integer id, Integer nivel, String name, String description, Integer parentId) {
        this.id = id;
        this.nivel = nivel;
        this.name = name;
        this.description = description;
        this.parentId = parentId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNivel() {
        return nivel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNivel(Integer nivel) {
        this.nivel = nivel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    @Override
    public String toString() {
        return "Hierarchy{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", nivel=" + nivel +
                ", parentId=" + parentId +
                '}';
    }
}
