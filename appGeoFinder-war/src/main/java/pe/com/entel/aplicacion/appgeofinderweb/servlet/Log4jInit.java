package pe.com.entel.aplicacion.appgeofinderweb.servlet;

import com.nextel.utils.ConfigurationPathPropertiesReaderFromEnvVariable;
import org.apache.log4j.PropertyConfigurator;
import pe.com.entel.aplicacion.appgeofinderweb.util.Constantes;


import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Properties;

public class Log4jInit extends HttpServlet {

    public void init() {
        Properties properties = new ConfigurationPathPropertiesReaderFromEnvVariable(
                new String[]{
                        Constantes.ENTEL_PROPERTIES,// configuration path
                        Constantes.APP_DIRECTORY,   // configuration directory
                        Constantes.LOG4J_PROPERTIES  // configuration file
                }
        ).load();
        PropertyConfigurator.configure(properties);
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) {
    }
}
