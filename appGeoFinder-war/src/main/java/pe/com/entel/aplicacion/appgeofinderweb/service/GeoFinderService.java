package pe.com.entel.aplicacion.appgeofinderweb.service;


import java.util.List;
import java.util.Map;

import pe.com.entel.aplicacion.appgeofinderweb.domain.AddressOfUse;
import pe.com.entel.aplicacion.appgeofinderweb.domain.Hierarchy;

public interface GeoFinderService {

    public void saveGeoFinderSearch(AddressOfUse addressOfUse);
    public void getBuildingName(Map<String, Object> mapRequest);
    public void getEncryptData(Map<String, Object> mapRequest);
    public void getKeyEncripted(Map<String, Object> mapRequest);
    public void getFlagEncrypt(Map<String, Object> mapRequest);

    // BEGIN: PRY-1443 - DOLANO
    public List<Hierarchy> getHierarchies();
    // END: PRY-1443 - DOLANO
}
