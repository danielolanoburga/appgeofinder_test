package pe.com.entel.aplicacion.appgeofinderweb.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import oracle.jdbc.driver.Const;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import pe.com.entel.aplicacion.appgeofinderweb.domain.*;
import pe.com.entel.aplicacion.appgeofinderweb.exception.GeoFinderException;
import pe.com.entel.aplicacion.appgeofinderweb.service.GeoFinderService;
import pe.com.entel.aplicacion.appgeofinderweb.servlet.ParametersInit;
import pe.com.entel.aplicacion.appgeofinderweb.util.Constantes;
import org.apache.log4j.Logger;


@Controller
public class GeoFinderController extends HttpServlet {

    private static final long serialVersionUID = -2795442251046990498L;

    private final Logger logger = Logger.getLogger(this.getClass());

    private List<KeyValue> keyValJer = null;
    //private boolean hasFlagEvalCoverage = false;

    @Autowired
    private GeoFinderService geoFinderService;

    @RequestMapping(value = "/initGeoFinder", method = RequestMethod.GET)
    public String initGeoFinder(
            @RequestParam(value = Constantes.C_PORTLET_EVAL_LOCATION_PARAM_APP_ORIGIN, required = false) String strAppOrigin,
            @RequestParam(value = Constantes.C_PORTLET_EVAL_LOCATION_PARAM_ADDRESS_TYPE, required = false) String strAddressType,
            @RequestParam(value = Constantes.C_PORTLET_EVAL_LOCATION_PARAM_DOC_TYPE, required = false) String strDocType,
            @RequestParam(value = Constantes.C_PORTLET_EVAL_LOCATION_PARAM_DOC_NUM, required = false) String strDocNum,
            @RequestParam(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_BUILDING_ID) int intBuildingId,
            @RequestParam(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_USER_LOGIN) String strUserLogin,
            @RequestParam(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_APP_ID) String strAppId,
            @RequestParam(value = Constantes.C_PORTLET_EVAL_LOCATION_PARAM_COD_TYPE_PROD, required = false) String codTypeProd, // PRY-1313 - DOLANO
            @RequestParam(value = Constantes.C_PORTLET_EVAL_LOCATION_PARAM_PHONE, required = false) String phone, // PRY-1313 - DOLANO
            ModelMap model) {

        try {
            logger.info("INICIA METODO :::::initGeoFinder::::::");
            logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_DOC_TYPE + ": " + strDocType);
            logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_DOC_NUM + ": " + strDocNum);
            logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_BUILDING_ID + ": " + intBuildingId);
            logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_USER_LOGIN + ": " + strUserLogin);
            logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_APP_ID + ": " + strAppId);
            logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_COD_TYPE_PROD + ": " + codTypeProd); // PRY-1313 - DOLANO
            logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_PHONE + ": " + phone); // PRY-1313 - DOLANO

            Map<String, Object> mapEncryptData = new HashMap<String, Object>();
            String keyEncrypted = null;

            geoFinderService.getFlagEncrypt(mapEncryptData);
            logger.info("Verify Flag Encrypt result: " + mapEncryptData.toString());
            if (mapEncryptData.get("mensaje") != null) {
                AddressOfUse addressOfUse = saveGeoFinderLogForErrors(strAppOrigin, strAddressType, strDocNum, strDocType, intBuildingId, strUserLogin,
                        Constantes.C_STATUS_GEOFINDER_ERROR, Constantes.C_ERROR_MESSAGE_BD_ENCRYPT + " - " +
                                mapEncryptData.get("mensaje").toString());

                String status = null;
                if (addressOfUse.getMensaje() != null) {
                    status = Constantes.C_STATUS_OK;
                } else {
                    status = Constantes.C_STATUS_ERROR;
                }
                throw new GeoFinderException(mapEncryptData.get("mensaje").toString(), status, addressOfUse);
            }

            int flagEncrypt = (Integer.parseInt(String.valueOf(mapEncryptData.get("numflagresult"))));
            logger.info("Flag de encriptacion: " + flagEncrypt);

            if (flagEncrypt == Constantes.C_ON) {
                //Obtenemos las llaves de encriptacion de la BD
                geoFinderService.getEncryptData(mapEncryptData);
                logger.info("Get Encrypt Data result: " + mapEncryptData.toString());

                if (mapEncryptData.get("mensaje") != null) {
                    AddressOfUse addressOfUse = saveGeoFinderLogForErrors(strAppOrigin, strAddressType, strDocNum, strDocType, intBuildingId, strUserLogin,
                            Constantes.C_STATUS_GEOFINDER_ERROR, Constantes.C_ERROR_MESSAGE_BD_ENCRYPT + " - " +
                                    mapEncryptData.get("mensaje").toString());
                    String status = null;
                    if (addressOfUse.getMensaje() != null) {
                        status = Constantes.C_STATUS_OK;
                    } else {
                        status = Constantes.C_STATUS_ERROR;
                    }
                    throw new GeoFinderException(mapEncryptData.get("mensaje").toString(), status, addressOfUse);
                }
                //Obtenemos el valor encryptado del WS
                mapEncryptData.put("appId", strAppId);
                mapEncryptData.put("appLogin", strUserLogin);
                mapEncryptData.put("idTx", System.currentTimeMillis());
                mapEncryptData.put("channel", Constantes.C_CHANNEL_WS);
                geoFinderService.getKeyEncripted(mapEncryptData);
                logger.info("Get Key Encrypted result: " + mapEncryptData.toString());

                if (mapEncryptData.get("mensaje") != null) {
                    AddressOfUse addressOfUse = saveGeoFinderLogForErrors(strAppOrigin, strAddressType, strDocNum, strDocType, intBuildingId, strUserLogin,
                            Constantes.C_STATUS_GEOFINDER_ERROR, Constantes.C_ERROR_MESSAGE_OSB_ENCRYPT + " - " +
                                    mapEncryptData.get("mensaje").toString());
                    String status = null;
                    if (addressOfUse.getMensaje() != null) {
                        status = Constantes.C_STATUS_OK;
                    } else {
                        status = Constantes.C_STATUS_ERROR;
                    }
                    throw new GeoFinderException(mapEncryptData.get("mensaje").toString(), status, addressOfUse);
                }
                keyEncrypted = String.valueOf(mapEncryptData.get("keyEncripted"));
            } else {
                keyEncrypted = Constantes.C_DEFAULT_KEY_GEOFINDER;
                ;
            }

            model.addAttribute(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_APP_ORIGIN, strAppOrigin);
            model.addAttribute(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_ADDRESS_TYPE, strAddressType);
            model.addAttribute(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_DOC_TYPE, strDocType);
            model.addAttribute(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_DOC_NUM, strDocNum);
            model.addAttribute(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_BUILDING_ID, intBuildingId);
            model.addAttribute(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_USER_LOGIN, strUserLogin);
            model.addAttribute(Constantes.C_PORTLET_GEOFINDER_KEY, keyEncrypted);
            // BEGIN: PRY-1313 - DOLANO
            model.addAttribute(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_COD_TYPE_PROD, codTypeProd);
            model.addAttribute(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_PHONE, phone);
            // END: PRY-1313 - DOLANO

            logger.info("TERMINA METODO :::::initGeoFinder::::::");
        } catch (GeoFinderException e) {
            logger.error("[GeoFinderException ERROR::::::" + e.getMessage() + "]", e);
            throw e;
        } catch (Exception e) {
            logger.error("[Exception e ERROR::::::" + e.getMessage() + "]", e);
            try {
                AddressOfUse addressOfUse = saveGeoFinderLogForErrors(strAppOrigin, strAddressType, strDocNum, strDocType, intBuildingId, strUserLogin,
                        Constantes.C_STATUS_GEOFINDER_ERROR, e.getMessage());
                String status = null;
                if (addressOfUse.getMensaje() != null) {
                    status = Constantes.C_STATUS_OK;
                } else {
                    status = Constantes.C_STATUS_ERROR;
                }
                throw new GeoFinderException(e.getMessage(), status, addressOfUse);
            } catch (Exception ex) {
                logger.error("[Exception ex ERROR::::::" + ex.getMessage() + "]", ex);
                throw new GeoFinderException(ex.getMessage());
            }

        }

        return "JP_GEOFINDER_show_page";

    }


    /*LGONZALES PRY-1140 de Se agrego el parametro de strAppOrigin y strAddressType */
    /*
    @RequestMapping(value = "/saveGeoFinderSearch", method = RequestMethod.POST)
    public @ResponseBody
    Response saveGeoFinderSearch(@RequestBody AddressOfUse addressOfUse,
                                 @RequestParam(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_APP_ORIGIN) String strAppOrigin,
                                 @RequestParam(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_ADDRESS_TYPE) String strAddressType,
                                 @RequestParam(value = Constantes.C_PORTLET_EVAL_LOCATION_PARAM_DOC_TYPE, required = false) String strDocType,
                                 @RequestParam(value = Constantes.C_PORTLET_EVAL_LOCATION_PARAM_DOC_NUM, required = false) String strDocNum,
                                 @RequestParam(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_BUILDING_ID) int intBuildingId,
                                 @RequestParam(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_USER_LOGIN) String strUserLogin ){
        logger.info("INICIA METODO :::::saveGeoFinderSearch::::::");
        logger.info("ADDRESSOFUSE: " + addressOfUse.toString());
        Response response = new Response();
        try{
            addressOfUse.setAppOrigen(strAppOrigin);
            addressOfUse.setTipoDireccion(strAddressType);
            addressOfUse.setNumDoc(strDocNum);
            addressOfUse.setTipoDoc(strDocType);
            addressOfUse.setNombreUsuario(strUserLogin);
            addressOfUse.setCodTienda(getCodBuilding(intBuildingId));

            if("".equals(addressOfUse.getStatus()) || addressOfUse.getStatus() == null){
                //LGONZALES PRY-1140
                if(addressOfUse.getZonas() != null && addressOfUse.getZonas().size()>0) {
                    boolean haveFlagEvalCoverage = false;
                    // BEGIN: PRY-1313 - DOLANO
                    String cobOutdoor = null;
                    String cobIndoor = null;
                    // END: PRY-1313 - DOLANO

                    for (Zone zone : addressOfUse.getZonas()) {
                        if(zone.getTipo().equals(Constantes.C_SITE_TYPE_COVERAGE)  && zone.getValor() != null && !"".equals(zone.getValor())){
                            haveFlagEvalCoverage=true;
                        }
                        if(zone.getTipo().equals(Constantes.C_SITE_TYPE_HSZ)){
                            addressOfUse.setHomeServiceZone(zone.getValor());
                        }
                        // BEGIN: PRY-1313 - DOLANO
                        if(zone.getTipo().equals(Constantes.C_SITE_TYPE_COD)){
                            cobOutdoor = zone.getValor();
                        }
                        if(zone.getTipo().equals(Constantes.C_SITE_TYPE_CID)){
                            cobIndoor = zone.getValor();
                        }
                        // END: PRY-1313 - DOLANO
                    }
                    if(haveFlagEvalCoverage){
                        // BEGIN: PRY-1313 - DOLANO
                        // Se comenta la siguiente linea (el flag 1 quedara como un historico en BD)
                        // addressOfUse.setFlagEvalCobertura(Constantes.C_STATUS_GEOFINDER_ZONE);
                        int flagEvalCov = Constantes.C_STATUS_GEOFINDER_ERROR;

                        String zoneCOD = cobOutdoor == null ? "" : cobOutdoor.trim().length() == 0 ? "" :
                                cobOutdoor.trim().equalsIgnoreCase(Constantes.C_UNDEFINED) ? "" : cobOutdoor;
                        String zoneCID = cobIndoor == null ? "" : cobIndoor.trim().length() == 0 ? "" :
                                cobIndoor.trim().equalsIgnoreCase(Constantes.C_UNDEFINED) ? "" : cobIndoor;

                        // Si existe COD y CID entonces es INDOOR_OUTDOOR
                        if(!zoneCOD.equals("") && !zoneCID.equals("")){
                            flagEvalCov = Constantes.C_STATUS_GEOFINDER_ZONE_OUTDOOR_INDOOR;
                        }
                        // Si existe solo COD entonces es OUTDOOR
                        else if(!zoneCOD.equals("") && zoneCID.equals("")){
                            flagEvalCov = Constantes.C_STATUS_GEOFINDER_ZONE_OUTDOOR;
                        }
                        // Si existe solo CID entonces es INDOOR
                        else if(zoneCOD.equals("") && !zoneCID.equals("")){
                            flagEvalCov = Constantes.C_STATUS_GEOFINDER_ZONE_INDOOR;
                        }

                        addressOfUse.setFlagEvalCobertura(flagEvalCov);
                        // END: PRY-1313 - DOLANO
                    }else{
                        addressOfUse.setFlagEvalCobertura(Constantes.C_STATUS_GEOFINDER_NO_ZONE);
                    }
                }else{
                    addressOfUse.setFlagEvalCobertura(Constantes.C_STATUS_GEOFINDER_NO_ZONE);
                }
            }else{
                if(String.valueOf(Constantes.C_STATUS_GEOFINDER_INVALID).equals(addressOfUse.getStatus())){
                    addressOfUse.setFlagEvalCobertura(Constantes.C_STATUS_GEOFINDER_INVALID);
                    addressOfUse.setMensajeErrorCobertura(Constantes.C_ERROR_MESSAGE_GF_INVALID);
                }else{
                    addressOfUse.setFlagEvalCobertura(Constantes.C_STATUS_GEOFINDER_ERROR);
                    addressOfUse.setMensajeErrorCobertura(addressOfUse.getStatus() + " - " + Constantes.C_ERROR_MESSAGE_GF_ERROR);
                }
            }
            saveGeoFinder(addressOfUse);
            
            response.setStatus(Constantes.C_STATUS_OK);            
        }catch (Exception e){
            logger.error("[Exception ERROR::::::" + e.getMessage() + "]", e);
            response.setStatus(Constantes.C_STATUS_ERROR);
            response.setMessage(e.getMessage());
        }
        response.setAddressOfUse(addressOfUse);
        logger.info("TERMINA METODO :::::saveGeoFinderSearch::::::");
        return response;
    }
    */

    private void saveGeoFinder(AddressOfUse addressOfUse) throws Exception {
        geoFinderService.saveGeoFinderSearch(addressOfUse);

        if (addressOfUse.getMensaje() != null) {
            throw new Exception(addressOfUse.getMensaje());
        }
    }

    /*LGONZALES PRY-1140*/
    private AddressOfUse saveGeoFinderLogForErrors(String strAppOrigin, String strAddressType, String strDocNum, String strDocType,
                                                   int intBuildingId, String strUserLogin, int flagEvalCoverage, String strMessage) {

        logger.info("INICIA METODO :::::saveGeoFinderLogForException::::::");
        logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_APP_ORIGIN + ": " + strAppOrigin);
        logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_ADDRESS_TYPE + ": " + strAddressType);
        logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_DOC_TYPE + ": " + strDocType);
        logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_DOC_NUM + ": " + strDocNum);
        logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_BUILDING_ID + ": " + intBuildingId);
        logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_USER_LOGIN + ": " + strUserLogin);
        logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_FLAG_EVAL_COVERAGE + ": " + flagEvalCoverage);
        logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_MESSAGE + ": " + strMessage);

        AddressOfUse addressOfUse = new AddressOfUse();
        addressOfUse.setAppOrigen(strAppOrigin);
        addressOfUse.setTipoDireccion(strAddressType);
        addressOfUse.setNumDoc(strDocNum);
        addressOfUse.setTipoDoc(strDocType);
        try {
            addressOfUse.setCodTienda(getCodBuilding(intBuildingId));
        } catch (Exception e) {
            logger.info("[ERROR::::::" + Constantes.C_ERROR_MESSAGE_GET_CODE_BUILDING + "]");
            logger.info("[ERROR::::::" + e.getMessage() + "]");
        }
        addressOfUse.setNombreUsuario(strUserLogin);
        addressOfUse.setFlagEvalCobertura(flagEvalCoverage);
        addressOfUse.setMensajeErrorCobertura(strMessage);

        logger.info("INICIA METODO :::::saveGeoFinderLogForException::::::");
        geoFinderService.saveGeoFinderSearch(addressOfUse);

        if (addressOfUse.getMensaje() != null) {
            logger.info("[ERROR::::::" + Constantes.C_ERROR_MESSAGE_LOG_SAVE + "]");
            logger.info("[ERROR::::::" + addressOfUse.getMensaje() + "]");
        } else {
            logger.info("[INFO::::::" + Constantes.C_SUCCESS_MESSAGE_LOG_SAVE + "]");
        }
        logger.info("FIN METODO :::::saveGeoFinderLogForException::::::");

        return addressOfUse;

    }

    private String getCodBuilding(int intBuildingId) throws Exception {
        Map<String, Object> mapBuilding = new HashMap<String, Object>();
        mapBuilding.put("buildingId", intBuildingId);
        geoFinderService.getBuildingName(mapBuilding);
        if (mapBuilding.get("mensaje") != null) {
            throw new Exception(mapBuilding.get("mensaje").toString());
        }
        return String.valueOf(mapBuilding.get("buildingName"));
    }

    /**
     * BEGIN: PRY-1443 - DOLANO
     */
    @RequestMapping(value = "/saveGeoFinderSearch", method = RequestMethod.POST)
    public
    @ResponseBody
    Response saveGeoFinderSearch(@RequestBody String jsonResponse,
                                 @RequestParam(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_APP_ORIGIN) String strAppOrigin,
                                 @RequestParam(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_ADDRESS_TYPE) String strAddressType,
                                 @RequestParam(value = Constantes.C_PORTLET_EVAL_LOCATION_PARAM_DOC_TYPE, required = false) String strDocType,
                                 @RequestParam(value = Constantes.C_PORTLET_EVAL_LOCATION_PARAM_DOC_NUM, required = false) String strDocNum,
                                 @RequestParam(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_BUILDING_ID) int intBuildingId,
                                 @RequestParam(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_USER_LOGIN) String strUserLogin) {
        logger.info("INICIA METODO :::::saveGeoFinderSearch::::::");
        logger.info("JSONRESPONSE: " + jsonResponse);
        logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_APP_ORIGIN + ": " + strAppOrigin);
        logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_ADDRESS_TYPE + ": " + strAddressType);
        logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_DOC_TYPE + ": " + strDocType);
        logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_DOC_NUM + ": " + strDocNum);
        logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_BUILDING_ID + ": " + intBuildingId);
        logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_USER_LOGIN + ": " + strUserLogin);
        logger.info(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_APP_ID + ": " + strAppOrigin);
        Response response = new Response();
        try {
            ObjectMapper mapper = new ObjectMapper();
            AddressOfUse addressOfUse = mapper.readValue(jsonResponse, AddressOfUse.class);
            addressOfUse.setJson(jsonResponse);
            addressOfUse.setAppOrigen(strAppOrigin);
            addressOfUse.setTipoDireccion(strAddressType);
            addressOfUse.setNumDoc(strDocNum);
            addressOfUse.setTipoDoc(strDocType);
            addressOfUse.setNombreUsuario(strUserLogin);
            addressOfUse.setCodTienda(getCodBuilding(intBuildingId));
            logger.info("addressOfUse: " + addressOfUse.toString());

            // Si NO devolvio algun status, es correcto
            if (addressOfUse.getStatus() == null || addressOfUse.getStatus().toString().equals("")) {
                // Si devolvio zonas
                if (addressOfUse.getZonas().size() > 0) {

                    for (Zona zona : addressOfUse.getZonas()) {
                        // Obtener valor HSZ del JSON
                        if (zona.getTipo().equalsIgnoreCase(Constantes.C_SITE_TYPE_HSZ)) {

                            if (zona.getAdditionalProperties().size() > 0) {
                                Map<String, Object> m = zona.getAdditionalProperties();
                                for (Map.Entry<String, Object> ma : m.entrySet()) {
                                    //hsz = ma.getValue().toString();
                                    addressOfUse.setHomeServiceZone(ma.getValue().toString());
                                }
                            }
                        }
                        // Obtener valor de zonas del JSON
                        else if (zona.getTipo().equalsIgnoreCase(Constantes.C_TYPE_RES)) {
                            keyValJer = new ArrayList<KeyValue>();
                            loopJerarquia(zona.getAdditionalProperties());
                        }
                    }

                    logger.info("[saveGeoFinderSearch] HSZ: " + addressOfUse.getHomeServiceZone());

                    // Obtener sites de JSON
                    List<Zona> listZonas = getZonasSites();
                    // Si existen zonas con cobertura como 2300 o 3500 o las que esten configuradas en la tabla herarquia (no AWS)
                    if (listZonas != null && listZonas.size() > 0) {
                        //hasFlagEvalCoverage = true;
                        Zona z = new Zona();
                        z.setTipo(Constantes.C_SITE_TYPE_HSZ);
                        z.setValor(addressOfUse.getHomeServiceZone());
                        listZonas.add(z);
                        addressOfUse.setFlagEvalCobertura(Constantes.C_STATUS_GEOFINDER_ZONE);
                        addressOfUse.setZonas(listZonas);
                    }
                    // Si no tiene cobertura en ninguna banda
                    else {
                        addressOfUse.setFlagEvalCobertura(Constantes.C_STATUS_GEOFINDER_NO_ZONE);
                    }
                }
                // Si no devolvio ninguna zona
                else {
                    addressOfUse.setFlagEvalCobertura(Constantes.C_STATUS_GEOFINDER_NO_ZONE);
                }
            }
            // Si el json devolvio algun status
            else {
                // Si devolvio status BUSQUEDA INVALIDA (-2)
                if (addressOfUse.getStatus().toString().equals(String.valueOf(Constantes.C_STATUS_GEOFINDER_INVALID))) {
                    addressOfUse.setFlagEvalCobertura(Constantes.C_STATUS_GEOFINDER_INVALID);
                    addressOfUse.setMensajeErrorCobertura(Constantes.C_ERROR_MESSAGE_GF_INVALID);
                }
                // Si devolvio otro tipo de error
                else {
                    addressOfUse.setFlagEvalCobertura(Constantes.C_STATUS_GEOFINDER_ERROR);
                    addressOfUse.setMensajeErrorCobertura(addressOfUse.getStatus().toString() + " - " + Constantes.C_ERROR_MESSAGE_GF_ERROR);
                }
            }

            // Si no tiene coberturas
            if (addressOfUse != null && addressOfUse.getFlagEvalCobertura() == Constantes.C_STATUS_GEOFINDER_NO_ZONE) {
                // Cuando no tiene coberturas solo se guardara HSZ en la tabla DET
                List<Zona> listZonas = new ArrayList<Zona>();
                Zona z = new Zona();
                z.setTipo(Constantes.C_SITE_TYPE_HSZ);
                z.setValor(addressOfUse.getHomeServiceZone());
                listZonas.add(z);
                addressOfUse.setZonas(listZonas);
            }

            addCovegares(addressOfUse); //portega PRY-1443

            // Guardar log
            saveGeoFinder(addressOfUse);
            logger.info("address: " + addressOfUse.toString());
            // Devolver OK al response
            response.setAddressOfUse(addressOfUse);
            response.setStatus(Constantes.C_STATUS_OK);
        } catch (Exception e) {
            logger.error("[Exception ERROR::::::" + e.getMessage() + "]", e);
            response.setStatus(Constantes.C_STATUS_ERROR);
            response.setMessage(e.getMessage());
        }
        logger.info("TERMINA METODO :::::saveGeoFinderSearch::::::");
        return response;
    }

    //portega PRY-1443
    private void addCovegares(AddressOfUse addressOfUse) {
        StringBuilder strResult = new StringBuilder();
        if (addressOfUse.getZonas() != null && addressOfUse.getZonas().size() > 0){
            for (Zona zona : addressOfUse.getZonas()) {
                if (zona.getHierarchyId()!=null && !zona.getHierarchyId().equals("")
                        && !zona.getHierarchyId().equals("null")){
                    strResult.append(zona.getHierarchyId()).append(",");
                    //logger.info("zona.getHierarchyId(): " + zona.getHierarchyId());
                    //logger.info("strResult1: " + strResult);
                }
            }
            //se elimina ultima coma
            //logger.info("strResult2: " + strResult);
            //logger.info("strResult.length(): " + strResult.length());
            if(strResult.length() > 0){
                strResult.delete(strResult.length() - 1, strResult.length());
            }
            addressOfUse.setCoverages(strResult.toString());
        }
    }

    private void loopJerarquia(Map<String, Object> map) {
        if (map.size() > 0) {
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                if (entry.getValue() instanceof ArrayList) {

                    List<Object> listObj = (List<Object>) entry.getValue();
                    if (listObj.size() > 0) {
                        for (Object obj : listObj) {
                            Map<String, Object> mapJer = (Map<String, Object>) obj;
                            for (Map.Entry<String, Object> jer : mapJer.entrySet()) {
                                if (jer.getValue() instanceof ArrayList) {
                                    getArray((List<Object>) jer.getValue());
                                } else {
                                    logger.info("[loopJerarquia] key: " + jer.getKey() + " - value: " + jer.getValue());
                                    logger.info(jer.getValue() == null ? "es null" : "no es null");
                                    keyValJer.add(new KeyValue(jer.getValue() == null ? "" : jer.getValue().toString(), jer.getKey()));
                                }
                            }
                        }
                    }
                } else {
                    logger.info("[loopJerarquia] key2: " + entry.getKey() + " - value2: " + entry.getValue());
                    keyValJer.add(new KeyValue(entry.getValue().toString(), entry.getKey()));
                }
            }
        }
    }

    private void getArray(List<Object> array) {
        if (array != null && array.size() > 0) {
            for (Object obj : array) {
                Map<String, Object> mapJer = (Map<String, Object>) obj;
                loopJerarquia(mapJer);
            }

        }
    }

    private List<Zona> getZonasSites() {
        List<String> sites = new ArrayList<String>();
        List<Zona> zonasSites = null;
        StringBuilder sb = new StringBuilder();
        boolean red = false;

        for (KeyValue kv : keyValJer) {
            logger.info("[getZonasSites] key: " + kv.getKey() + ", value: " + kv.getValue());
            Integer niv = getHierarchyLevel(kv.getValue());
            logger.info("[getZonasSites] niv: " + niv);
            if (niv != null && niv == 1) {
                if (red) {
                    sb.append("&");
                }
                sb.append(kv.getKey() + "," + kv.getValue());
                red = true;
            } else {
                sb.append("|" + kv.getKey() + "," + kv.getValue());
            }
        }

        logger.info("[getZonasSites] sb: " + sb.toString());

        if (!sb.toString().trim().isEmpty()) {
            StringBuilder sb2 = new StringBuilder();
            String[] list = sb.toString().split("&");
            for (String s : list) {
                logger.info("s: " + s);
                if (!s.trim().isEmpty()) {
                    // Si la banda no tiene hijos
                    if (s.indexOf("|") == -1) {
                        continue;
                    }
                    // Extraer nombre de la banda
                    String r = s.substring(s.indexOf(",") + 1, s.indexOf("|"));
                    logger.info("r: " + r);
                    Hierarchy jer = getHierarchy(r, null);
                    Integer parentId = jer.getId();
                    logger.info("parentId: " + parentId);
                    if (jer != null) {
                        sb2 = new StringBuilder();
                        sb2.append(jer.getId() + "," + r);
                        String lineHijos = s.substring(s.indexOf("|") + 1);
                        logger.info("lineHijos: " + lineHijos);
                        String[] hijos = lineHijos.split("\\|");
                        boolean noConfigured = false;

                        //for (int h = hijos.length - 1; h >= 0; h--) {
                        for (int h = 0; h < hijos.length; h++) {
                            logger.info("------------------");
                            logger.info("h: " + hijos[h]);
                            if (!hijos[h].trim().isEmpty()) {
                                String hijoKey = hijos[h].substring(0, hijos[h].indexOf(","));
                                logger.info("hijoKey: " + hijoKey);
                                String hijo = hijos[h].substring(hijos[h].indexOf(",") + 1);
                                logger.info("hijoValue: " + hijo);
                                logger.info("parentId: " + parentId);

                                // Si es SITE
                                if (hijoKey.equalsIgnoreCase(Constantes.C_TYPE_SITE) && !noConfigured) {
                                    sb2.append("=" + hijo);
                                    continue;
                                }

                                // Obtener jerarquia
                                jer = getHierarchy(hijo, parentId);
                                if (jer != null) {
                                    noConfigured = false;
                                    sb2.append("|" + jer.getId() + "," + hijo);
                                }
                                // Si no tiene jerarquia configurada
                                else {
                                    noConfigured = true;
                                }
                            }
                        }
                        logger.info("sb2: " + sb2.toString());
                        sites.add(sb2.toString());
                    }
                }
            }

            if (sites != null && sites.size() > 0) {
                //List<String> finalSites = new ArrayList<String>();
                zonasSites = new ArrayList<Zona>();
                for (String s : sites) {
                    logger.info("s: " + s);
                    String[] splitSites = s.split("\\|");
                    if (splitSites.length > 0) {
                        //splitSites[0];  // RED
                        for (int i = 0; i < splitSites.length; i++) {
                            if (i > 0) {
                                logger.info("sp: " + splitSites[i]);
                                String[] splitCobs = splitSites[i].split(",");
                                if (splitCobs.length > 1) {
                                    logger.info("indOf: " + splitCobs[1].indexOf("="));
                                    if (splitCobs[1].indexOf("=") > -1) {
                                        // Si hay valor de site o es diferente de UNDEFINED
                                        if (!splitCobs[1].substring(splitCobs[1].indexOf("=") + 1).trim().equals("")
                                                || splitCobs[1].substring(splitCobs[1].indexOf("=") + 1).equalsIgnoreCase(Constantes.C_UNDEFINED)) {
                                            // agregar a ZONA
                                            //finalSites.add(splitSites[i]);
                                            Zona z = new Zona();
                                            z.setHierarchyId(Integer.parseInt(splitSites[i].substring(0, splitSites[i].indexOf(","))));
                                            //z.setTipo(splitSites[i].substring(splitSites[i].indexOf(",") + 1, splitSites[i].indexOf("=")));
                                            z.setTipo(Constantes.C_SITE_TYPE_COVERAGE);
                                            z.setValor(splitSites[i].substring(splitSites[i].indexOf("=") + 1));
                                            logger.info("zone toString: " + z.toString());
                                            zonasSites.add(z);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return zonasSites;
    }

    private Integer getHierarchyLevel(String value) {
        logger.info("[getHierarchyLevel] value: " + value);
        if (ParametersInit.hierarchies != null && ParametersInit.hierarchies.size() > 0) {
            for (Hierarchy h : ParametersInit.hierarchies) {
                //logger.info("[getHierarchyLevel] h: " + h.toString());
                //if (h.getDescription().equalsIgnoreCase(value)) {
                if (h.getName().equalsIgnoreCase(value)) {
                    return h.getNivel();
                }
            }
        }
        return null;
    }

    public Hierarchy getHierarchy(String value, Integer parentId) {
        logger.info("[getHierarchy] value: " + value + ", parentId: " + parentId);
        Hierarchy hierarchy = null;
        if (value != null && !value.trim().isEmpty()) {
            for (Hierarchy h : ParametersInit.hierarchies) {
                logger.info("[getHierarchy] h: " + h.toString());
                //if (h.getDescription().equalsIgnoreCase(value) && h.getParentId() == parentId) {
                if (h.getName().equalsIgnoreCase(value) && h.getParentId() == parentId) {
                    hierarchy = h;
                    break;
                }
            }
        }
        return hierarchy;
    }

    /** END: PRY-1443 - DOLANO */

}
