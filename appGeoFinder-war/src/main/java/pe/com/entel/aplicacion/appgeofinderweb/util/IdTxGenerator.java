package pe.com.entel.aplicacion.appgeofinderweb.util;

public class IdTxGenerator {

    private static final ThreadLocal<String> context = new ThreadLocal<String>();
    private static final ThreadLocal<String> contextBT = new ThreadLocal<String>();

    public static void startTransaction() {
        if(getTransactionId() == null){
            String idTx = String.valueOf(System.currentTimeMillis());
            context.set(idTx);
        }
    }

    public static void startTransactionBT(String id) {
        contextBT.set(id);
    }

    public static String getTransactionId() {
        return context.get();
    }

    public static String getTransactionBTId() {
        return contextBT.get();
    }

    public static void endTransaction() {
        context.remove();
        contextBT.remove();
    }

}
