package pe.com.entel.aplicacion.appgeofinderweb.domain;

/**
 * Created by JTALLEDO on 22/03/2018.
 */
public class Response {

    private String status;
    private String message;
    private AddressOfUse addressOfUse; //LGONZALES PRY-1140

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AddressOfUse getAddressOfUse() {
        return addressOfUse;
    }

    public void setAddressOfUse(AddressOfUse addressOfUse) {
        this.addressOfUse = addressOfUse;
    }
}
