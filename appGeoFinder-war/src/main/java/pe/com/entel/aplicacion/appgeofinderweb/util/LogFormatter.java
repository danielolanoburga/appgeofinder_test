package pe.com.entel.aplicacion.appgeofinderweb.util;

public class LogFormatter {

    private static String separator = "|";

    public static String format(Object message) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(Constantes.APP_DIRECTORY).append(separator).append("- [idTx=").append(IdTxGenerator.getTransactionId()).append("]").append(separator).append(message);
        return stringBuilder.toString();
    }

}
