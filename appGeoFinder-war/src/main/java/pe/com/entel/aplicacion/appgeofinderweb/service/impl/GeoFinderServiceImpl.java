package pe.com.entel.aplicacion.appgeofinderweb.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.entel.aplicacion.appgeofinderweb.dao.GeoFinderMapper;
import pe.com.entel.aplicacion.appgeofinderweb.domain.AddressOfUse;
import pe.com.entel.aplicacion.appgeofinderweb.domain.Hierarchy;
import pe.com.entel.aplicacion.appgeofinderweb.service.GeoFinderService;
import pe.com.entel.aplicacion.appgeofinderweb.util.PropertiesConfig;
import pe.com.entel.esb.data.generico.entelgenericheader.v1.HeaderRequestType;
import pe.com.entel.esb.data.generico.entelgenericheader.v1.HeaderResponseType;
import pe.com.entel.esb.message.encrypt.getencrypt.v1.GetEncryptRequest;
import pe.com.entel.esb.message.encrypt.getencrypt.v1.GetEncryptResponse;
import pe.com.entel.esb.product.encrypt.v1.Encrypt;
import pe.com.entel.esb.product.encrypt.v1.EncryptService;


import java.net.URL;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.xml.ws.Holder;

@Service
public class GeoFinderServiceImpl implements GeoFinderService{

    private Logger logger = Logger.getLogger(getClass());

    @Resource
    private GeoFinderMapper geoFinderMapper;

    @Autowired
    private PropertiesConfig propertiesConfig;

    @Override
    public void saveGeoFinderSearch(AddressOfUse addressOfUse) {
        geoFinderMapper.saveGeoFinderSearch(addressOfUse);
    }

	@Override
	public void getBuildingName(Map<String, Object> mapRequest) {
		geoFinderMapper.getBuildingName(mapRequest);
	}

	@Override
	public void getEncryptData(Map<String, Object> mapRequest) {
		geoFinderMapper.getEncryptData(mapRequest);
		
	}

    @Override
    public void getKeyEncripted(Map<String, Object> mapRequest) {
        try {
            logger.info("Inicia getKeyEncripted");
            HeaderRequestType headerRequestType=new HeaderRequestType();
            headerRequestType.setIdAplicacion(Long.parseLong(mapRequest.get("appId").toString()));
            headerRequestType.setUsuario(mapRequest.get("appLogin").toString());
            headerRequestType.setIdTransaccionNegocio(mapRequest.get("idTx").toString());
            headerRequestType.setCanal(mapRequest.get("channel").toString());
            Holder<HeaderResponseType> typeHolder=new Holder<HeaderResponseType>();

            URL endPointEncrypt = new URL(propertiesConfig.getWsdlEncryptLocation());
            Encrypt encrypt = new EncryptService(endPointEncrypt).getEncryptPort();
            GetEncryptRequest getEncryptRequest = new GetEncryptRequest();
            getEncryptRequest.setKey(mapRequest.get("encryptKey").toString());
            GetEncryptResponse getEncryptResponse = encrypt.getEncrypt(getEncryptRequest, headerRequestType, typeHolder);
            mapRequest.put("keyEncripted",getEncryptResponse.getResponseData().getKeyEncrypted());
        } catch (Exception e) {
            logger.error(e);
            mapRequest.put("mensaje", e.getMessage());
        }
        logger.info("Termina getKeyEncripted");
    }

    @Override
    public void getFlagEncrypt(Map<String, Object> mapRequest) {
        geoFinderMapper.getFlagEncrypt(mapRequest);
    }

    // BEGIN: PRY-1443 - DOLANO
    @Override
    public List<Hierarchy> getHierarchies() {
        return geoFinderMapper.getHierarchies();
    }
    // END: PRY-1443 - DOLANO
}
