package pe.com.entel.aplicacion.appgeofinderweb.domain;

import java.io.Serializable;

/**
 * Created by JTALLEDO on 22/03/2018.
 * Edited by DOLANO - PRY-1443 on 22/05/2019.
 */
public class Zone implements Serializable{
	
	private static final long serialVersionUID = 2873603759339920109L;
    //LGONZALES PRY-1140
	private String tipo;
    private String valor;
    private Integer hierarchyId;

    public Zone(){}

    public String getTipo() {
        return tipo;
    }

    public String getValor() {
        return valor;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Integer getHierarchyId() {
        return hierarchyId;
    }

    public void setHierarchyId(Integer hierarchyId) {
        this.hierarchyId = hierarchyId;
    }
}
