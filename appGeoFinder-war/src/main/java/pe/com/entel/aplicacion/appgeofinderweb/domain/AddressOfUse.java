package pe.com.entel.aplicacion.appgeofinderweb.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by JTALLEDO on 22/03/2018.
 * Edited by DOLANO - PRY-1443
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "status",
        "clase_hu",
        "cod_hu",
        "cod_via",
        "departamento",
        "distrito",
        "km",
        "lote",
        "mz",
        "nom_hu",
        "nom_via",
        "numero",
        "provincia",
        "tipo_via",
        "ubigeo",
        "x",
        "y",
        "zonas"
})
public class AddressOfUse implements Serializable {

    private static final long serialVersionUID = 5382673581027931986L;

    @JsonProperty("status")
    private Object status;
    @JsonProperty("clase_hu")
    private String clase_hu;
    @JsonProperty("cod_hu")
    private String cod_hu;
    @JsonProperty("cod_via")
    private String cod_via;
    @JsonProperty("departamento")
    private String departamento;
    @JsonProperty("distrito")
    private String distrito;
    @JsonProperty("km")
    private String km;
    @JsonProperty("lote")
    private Object lote;
    @JsonProperty("mz")
    private Object mz;
    @JsonProperty("nom_hu")
    private String nom_hu;
    @JsonProperty("nom_via")
    private String nom_via;
    @JsonProperty("numero")
    private String numero;
    @JsonProperty("provincia")
    private String provincia;
    @JsonProperty("tipo_via")
    private String tipo_via;
    @JsonProperty("ubigeo")
    private String ubigeo;
    @JsonProperty("x")
    private String x;
    @JsonProperty("y")
    private String y;
    @JsonProperty("zonas")
    private List<Zona> zonas = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    private Zone zona;
    private String homeServiceZone; //LGONZALES PRY-1140

    private String codTienda;
    private String tipoDireccion;
    private String appOrigen;
    private String tipoDoc;
    private String numDoc;
    private String nombreUsuario;
    private int flagEvalCobertura;
    private String mensajeErrorCobertura;

    private String mensaje;
    private Long evalCoverageId; // DOLANO PRY-1313
    //@JsonIgnore
    private String coverages; //portega PRY-1443

    private String json; // DOLANO PRY-1443

    @JsonProperty("status")
    public Object getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(Object status) {
        this.status = status;
    }

    @JsonProperty("clase_hu")
    public String getClase_hu() {
        return clase_hu;
    }

    @JsonProperty("clase_hu")
    public void setClase_hu(String clase_hu) {
        this.clase_hu = clase_hu;
    }

    @JsonProperty("cod_hu")
    public String getCod_hu() {
        return cod_hu;
    }

    @JsonProperty("cod_hu")
    public void setCod_hu(String cod_hu) {
        this.cod_hu = cod_hu;
    }

    @JsonProperty("cod_via")
    public String getCod_via() {
        return cod_via;
    }

    @JsonProperty("cod_via")
    public void setCod_via(String cod_via) {
        this.cod_via = cod_via;
    }

    @JsonProperty("departamento")
    public String getDepartamento() {
        return departamento;
    }

    @JsonProperty("departamento")
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    @JsonProperty("distrito")
    public String getDistrito() {
        return distrito;
    }

    @JsonProperty("distrito")
    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    @JsonProperty("km")
    public String getKm() {
        return km;
    }

    @JsonProperty("km")
    public void setKm(String km) {
        this.km = km;
    }

    @JsonProperty("lote")
    public Object getLote() {
        return lote;
    }

    @JsonProperty("lote")
    public void setLote(Object lote) {
        this.lote = lote;
    }

    @JsonProperty("mz")
    public Object getMz() {
        return mz;
    }

    @JsonProperty("mz")
    public void setMz(Object mz) {
        this.mz = mz;
    }

    @JsonProperty("nom_hu")
    public String getNom_hu() {
        return nom_hu;
    }

    @JsonProperty("nom_hu")
    public void setNom_hu(String nom_hu) {
        this.nom_hu = nom_hu;
    }

    @JsonProperty("nom_via")
    public String getNom_via() {
        return nom_via;
    }

    @JsonProperty("nom_via")
    public void setNom_via(String nom_via) {
        this.nom_via = nom_via;
    }

    @JsonProperty("numero")
    public String getNumero() {
        return numero;
    }

    @JsonProperty("numero")
    public void setNumero(String numero) {
        this.numero = numero;
    }

    @JsonProperty("provincia")
    public String getProvincia() {
        return provincia;
    }

    @JsonProperty("provincia")
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    @JsonProperty("tipo_via")
    public String getTipo_via() {
        return tipo_via;
    }

    @JsonProperty("tipo_via")
    public void setTipo_via(String tipo_via) {
        this.tipo_via = tipo_via;
    }

    @JsonProperty("ubigeo")
    public String getUbigeo() {
        return ubigeo;
    }

    @JsonProperty("ubigeo")
    public void setUbigeo(String ubigeo) {
        this.ubigeo = ubigeo;
    }

    @JsonProperty("x")
    public String getX() {
        return x;
    }

    @JsonProperty("x")
    public void setX(String x) {
        this.x = x;
    }

    @JsonProperty("y")
    public String getY() {
        return y;
    }

    @JsonProperty("y")
    public void setY(String y) {
        this.y = y;
    }

    @JsonProperty("zonas")
    public List<Zona> getZonas() {
        return zonas;
    }

    @JsonProperty("zonas")
    public void setZonas(List<Zona> zonas) {
        this.zonas = zonas;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Zone getZona() {
        return zona;
    }

    public void setZona(Zone zona) {
        this.zona = zona;
    }

    public String getCodTienda() {
        return codTienda;
    }

    public void setCodTienda(String codTienda) {
        this.codTienda = codTienda;
    }

    public String getTipoDireccion() {
        return tipoDireccion;
    }

    public void setTipoDireccion(String tipoDireccion) {
        this.tipoDireccion = tipoDireccion;
    }

    public String getAppOrigen() {
        return appOrigen;
    }

    public void setAppOrigen(String appOrigen) {
        this.appOrigen = appOrigen;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public String getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public int getFlagEvalCobertura() {
        return flagEvalCobertura;
    }

    public void setFlagEvalCobertura(int flagEvalCobertura) {
        this.flagEvalCobertura = flagEvalCobertura;
    }

    public String getMensajeErrorCobertura() {
        return mensajeErrorCobertura;
    }

    public void setMensajeErrorCobertura(String mensajeErrorCobertura) {
        this.mensajeErrorCobertura = mensajeErrorCobertura;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getHomeServiceZone() {
        return homeServiceZone;
    }

    public void setHomeServiceZone(String homeServiceZone) {
        this.homeServiceZone = homeServiceZone;
    }

    // BEGIN: PRY-1313 - DOLANO
    public Long getEvalCoverageId() {
        return evalCoverageId;
    }

    public void setEvalCoverageId(Long evalCoverageId) {
        this.evalCoverageId = evalCoverageId;
    }
    // END: PRY-1313 - DOLANO

    // BEGIN: PRY-1443 - DOLANO
    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }
    // BEGIN: PRY-1443 - DOLANO


    public String getCoverages() {
        return coverages;
    }

    public void setCoverages(String coverages) {
        this.coverages = coverages;
    }

    @Override
    public String toString() {
        return "AddressOfUse{" +
                "status=" + status +
                ", clase_hu='" + clase_hu + '\'' +
                ", cod_hu='" + cod_hu + '\'' +
                ", cod_via='" + cod_via + '\'' +
                ", departamento='" + departamento + '\'' +
                ", distrito='" + distrito + '\'' +
                ", km='" + km + '\'' +
                ", lote=" + lote +
                ", mz=" + mz +
                ", nom_hu='" + nom_hu + '\'' +
                ", nom_via='" + nom_via + '\'' +
                ", numero='" + numero + '\'' +
                ", provincia='" + provincia + '\'' +
                ", tipo_via='" + tipo_via + '\'' +
                ", ubigeo='" + ubigeo + '\'' +
                ", x='" + x + '\'' +
                ", y='" + y + '\'' +
                ", zonas=" + zonas +
                ", additionalProperties=" + additionalProperties +
                ", zona=" + zona +
                ", homeServiceZone='" + homeServiceZone + '\'' +
                ", codTienda='" + codTienda + '\'' +
                ", tipoDireccion='" + tipoDireccion + '\'' +
                ", appOrigen='" + appOrigen + '\'' +
                ", tipoDoc='" + tipoDoc + '\'' +
                ", numDoc='" + numDoc + '\'' +
                ", nombreUsuario='" + nombreUsuario + '\'' +
                ", flagEvalCobertura=" + flagEvalCobertura +
                ", mensajeErrorCobertura='" + mensajeErrorCobertura + '\'' +
                ", mensaje='" + mensaje + '\'' +
                ", evalCoverageId=" + evalCoverageId +
                '}';
    }
}