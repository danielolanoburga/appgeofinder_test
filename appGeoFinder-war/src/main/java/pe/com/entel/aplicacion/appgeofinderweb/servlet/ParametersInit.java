package pe.com.entel.aplicacion.appgeofinderweb.servlet;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.com.entel.aplicacion.appgeofinderweb.domain.Hierarchy;
import pe.com.entel.aplicacion.appgeofinderweb.service.GeoFinderService;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.util.List;

/**
 * Created by DOLANO - PRY-1443 on 20/05/2019.
 */
@Component
public class ParametersInit {

    private final Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    GeoFinderService geoFinderService;

    public static List<Hierarchy> hierarchies;

    @PostConstruct
    public void init() throws ServletException {
        logger.info("[ParametersInit] INIT ParametersConfig");
        hierarchies = geoFinderService.getHierarchies();
        for (Hierarchy h : ParametersInit.hierarchies) {
            logger.info("[ParametersInit] h: " + h.toString());
        }
        logger.info("[ParametersInit] hierarchies size: " + hierarchies.size());
    }
}
