package pe.com.entel.aplicacion.appgeofinderweb.handler;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import org.apache.ibatis.type.JdbcType;
import pe.com.entel.aplicacion.appgeofinderweb.domain.Zona;
import pe.com.entel.aplicacion.appgeofinderweb.domain.Zone;


import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//LGONZALES PRY-1140
public class TypeZoneHandler extends GenericTypeHandler {

    protected String schema;

    // BEGIN: DOLANO - PRY-1443
    @Override
    public void setParameter(PreparedStatement preparedStatement, int i, Object o, JdbcType jdbcType) throws SQLException {
        List<Zona> zoneList = (List<Zona>) o;
        StructDescriptor structDescriptor = StructDescriptor.createDescriptor(schema + ".TO_ZONE", preparedStatement.getConnection());
        List<STRUCT> structs = new ArrayList<STRUCT>();
        if (zoneList != null) {
            for (Zona zona : zoneList) {
                Object[] zoneObject = {
                        zona.getTipo(),
                        zona.getValor(),
                        zona.getHierarchyId()
                };
                STRUCT struct = new STRUCT(structDescriptor, preparedStatement.getConnection(), zoneObject);
                structs.add(struct);
            }
        }
        ArrayDescriptor arrayDescriptor = ArrayDescriptor.createDescriptor(schema + ".TT_ZONE", preparedStatement.getConnection());
        ARRAY array = new ARRAY(arrayDescriptor, preparedStatement.getConnection(), structs.toArray());
        preparedStatement.setObject(i, array, jdbcType.TYPE_CODE);
    }
    // END: DOLANO - PRY-1443

    @Override
    public Object getResult(ResultSet resultSet, String s) throws SQLException {
        return null;
    }

    @Override
    public Object getResult(ResultSet resultSet, int i) throws SQLException {
        return null;
    }

    @Override
    public Object getResult(CallableStatement callableStatement, int index) throws SQLException {
        return null;
    }
}
