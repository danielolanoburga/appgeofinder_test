package pe.com.entel.aplicacion.appgeofinderweb.dao;

import java.util.List;
import java.util.Map;

import pe.com.entel.aplicacion.appgeofinderweb.domain.AddressOfUse;
import pe.com.entel.aplicacion.appgeofinderweb.domain.Hierarchy;

/**
 * Created by JTALLEDO on 22/03/2018.
 */
public interface GeoFinderMapper {

    public void saveGeoFinderSearch(AddressOfUse addressOfUse);
    public Map<String, Object> getBuildingName(Map<String, Object> mapRequest);
    public Map<String, Object> getEncryptData(Map<String, Object> mapRequest);
    public Map<String, Object> getFlagEncrypt(Map<String, Object> mapRequest);

    // BEGIN: PRY-1443 - DOLANO
    public List<Hierarchy> getHierarchies();
    // END: PRY-1443 - DOLANO

}
