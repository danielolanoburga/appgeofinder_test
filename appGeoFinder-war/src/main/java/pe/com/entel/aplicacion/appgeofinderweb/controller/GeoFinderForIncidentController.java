package pe.com.entel.aplicacion.appgeofinderweb.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import pe.com.entel.aplicacion.appgeofinderweb.service.GeoFinderService;
import pe.com.entel.aplicacion.appgeofinderweb.util.Constantes;

import javax.servlet.http.HttpServlet;


//LGONZALES PRY-1140
@Controller
public class GeoFinderForIncidentController extends HttpServlet {
	
	private static final long serialVersionUID = -2795442251046990498L;

	private  final Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private GeoFinderService geoFinderService;

    @RequestMapping(value = "/initGeoFinderForIncident", method = RequestMethod.GET)
    public String initGeoFinderForIncident(
            @RequestParam(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_APP_ORIGIN) String strAppOrigin,
            @RequestParam(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_ADDRESS_TYPE) String strAddressType,
            @RequestParam(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_BUILDING_ID) String intBuildingId,
            @RequestParam(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_APP_ID) String strAppId,
            @RequestParam(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_USER_LOGIN) String strUserLogin,
            @RequestParam(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_COD_TYPE_PROD) String codTypeProd, // PRY-1313 - DOLANO
            @RequestParam(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_PHONE) String phone, // PRY-1313 - DOLANO
            ModelMap model) {

        model.addAttribute("dynamicFrmGeoFinderComponent", loadGeoFinderComponent(strAppOrigin, strAddressType, intBuildingId,
                strAppId, strUserLogin,
                codTypeProd, phone // PRY-1313 - DOLANO
                ).toString());

        return "JP_GEOFINDER_FOR_INCIDENT_show_page";
    }

    private StringBuilder loadGeoFinderComponent(String strAppOrigin, String strAddressType, String intBuildingId,
                                                String strAppId, String strUserLogin,
                                                String codTypeProd, String phone // PRY-1313 - DOLANO
                                                ) {
        StringBuilder sb = new StringBuilder();
        sb.append("<iframe id=\"locationFrame\" name=\"locationFrame\" src=\"");
        sb.append(Constantes.C_GEOFINDER_INIT_URL);
        sb.append("?");
        sb.append(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_APP_ORIGIN);
        sb.append("=");
        sb.append(strAppOrigin);
        sb.append("&");
        sb.append(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_ADDRESS_TYPE);
        sb.append("=");
        sb.append(strAddressType);
        sb.append("&");
        sb.append(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_BUILDING_ID);
        sb.append("=");
        sb.append(intBuildingId);
        sb.append("&");
        sb.append(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_USER_LOGIN);
        sb.append("=");
        sb.append(strUserLogin);
        sb.append("&");
        sb.append(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_APP_ID);
        sb.append("=");
        sb.append(strAppId);
        // BEGIN: PRY-1313 - DOLANO
        sb.append("&");
        sb.append(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_COD_TYPE_PROD);
        sb.append("=");
        sb.append(codTypeProd);
        sb.append("&");
        sb.append(Constantes.C_PORTLET_EVAL_LOCATION_PARAM_PHONE);
        sb.append("=");
        sb.append(phone);
        // END: PRY-1313 - DOLANO
        sb.append("\" width=\"100%\" height=\"500px\"></iframe>");
        return sb;
    }

}
