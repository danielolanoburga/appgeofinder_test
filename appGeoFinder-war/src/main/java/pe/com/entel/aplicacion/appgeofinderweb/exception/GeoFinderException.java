package pe.com.entel.aplicacion.appgeofinderweb.exception;

import pe.com.entel.aplicacion.appgeofinderweb.domain.AddressOfUse;

import java.util.Date;

public class GeoFinderException extends RuntimeException{
	
	private static final long serialVersionUID = -5122020032755191607L;
	
	private Date date;
    private String message;
    private String status;
    private AddressOfUse addressOfUse;//LGONZALES PRY-1140
     
    public GeoFinderException(String message) {
        super();
        this.date = new Date();
        this.message = message;
    }
    //LGONZALES PRY-1140
    public GeoFinderException(String message,String status, AddressOfUse addressOfUse) {
        super();
        this.date = new Date();
        this.message = message;
        this.status = status;
        this.addressOfUse = addressOfUse;
    }
 
    public Date getDate() {
        return date;
    }
 
    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public AddressOfUse getAddressOfUse() {
        return addressOfUse;
    }

    @Override
    public String toString() {
            return "GeoFinderException [date=" + date + ", message=" + message + "]";
        }

}
