<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="ISO-8859-1"%>
<%@ include file="../commons/importCommons.jsp" %>

<html>
<spring:url value="/resources/geofinderjs/js/geofinderjs.js" var="geoFinderJS"/>
<spring:url value="/resources/geofinderjs/css/geofinderjs.css" var="geoFinderCSS"/>
<spring:url value="/resources/geofinderjs/css/gfpersonalizado.css" var="gfPersonalizadoCSS"/>

<spring:url value="/saveGeoFinderSearch" var="saveGeoFinderSearchURL">
    <spring:param name="prmAppOrigin" value="${prmAppOrigin}"/>
    <spring:param name="prmAddressType" value="${prmAddressType}"/>
    <spring:param name="prmDocNum" value="${prmDocNum}"/>
    <spring:param name="prmDocType" value="${prmDocType}"/>
    <spring:param name="prmBuildingId" value="${prmBuildingId}"/>
    <spring:param name="prmUserLogin" value="${prmUserLogin}"/>
</spring:url>

<head>
    <title>GEOFINDER</title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Estilo de Jquery (REQUERIDO) -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
    <!-- Carga de Jquery UI (REQUERIDO) -->
    <script src="//code.jquery.com/ui/1.8.24/jquery-ui.js"></script>
    <!-- Carga de GeoFinder JS-->
    <script src="${geoFinderJS}"></script>
    <!-- Estilo de Jquery UI -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- Estilo BASE de GeofinderJS -->
    <link rel="stylesheet" href="${geoFinderCSS}"/>
    <!-- Estilo personalizado -->
    <link rel="stylesheet" href="${gfPersonalizadoCSS}"/>
</head>
<body>
<div id="formcont" style="width: 35%;">
    <div id="form">
    </div>
    <div id="bcont">
        <input type="submit" id="guardar" value="Guardar Direcci�n" style="visibility: hidden"/>
    </div>
</div>
<div id="map" style="width: 65%;">
</div>
</body>

<script type="text/javascript">
    //capas de intersecci�n
    // BEGIN: DOLANO - PRY-1443
    var capas = Array('site_entel');
    //var capas = Array('amtel_i_2300','amtel_o_2300','amtel_i_3500','amtel_o_3500');
    var key = "<c:out value='${prmGeoFinderKey}'/>";
    //var key = "3zf307fcacqbks8k9aorklf6ayt17co6";
    //var key = "zm184615uef8zlbscyrnzt0q9yb940qr";
    // END: DOLANO - PRY-1443

    direccion = {};

    geofinderjs.visualizar("map", "form", key, true, false, capas, direccion, saveLog);

    //ESTO SIRVE PARA DETECTAR QUE EL RESULTADO DE LA BUSQUEDA SEA CORRECTO Y VISUALIZAR EL LINK DE CREAR INCIDENTE
    var target = document.querySelector('#gf_zone_alert')
    var observer = new MutationObserver(function(mutations) {
        if(self.parent != null && self.parent != undefined){
            var objIncidentLink = self.parent.document.querySelector('#incident');
            if(objIncidentLink){
                if($('#gf_zone_alert').text().toString().toUpperCase() != 'NO LOCALIZADO'){
                    objIncidentLink.style.visibility="visible";
                }else{
                    objIncidentLink.style.visibility="hidden";
                }
            }
        }


    });
    var config = { attributes: true, childList: true, characterData: true };
    observer.observe(target, config);
    //END
    //Funciones personalizadas
    function saveLog(objResult) {
        //var objResult = geofinderjs.resultado;
        console.log(objResult);
        //var btnGuardar =$('#guardar');
        var appOrigin = "<c:out value="${prmAppOrigin}"/>";
        if( appOrigin == "<%=Constantes.C_PORTLET_EVAL_LOCATION_PARAM_INCIDENT_ORIGIN_VALUE%>"){
            cleanHdnAddress();
        }

        var dataString = {
            status: objResult.status,
            clase_hu: objResult.clase_hu,
            cod_hu: objResult.cod_hu,
            cod_via: objResult.cod_via,
            departamento: objResult.departamento,
            distrito: objResult.distrito,
            km: objResult.km,
            lote: objResult.lote,
            mz: objResult.mz,
            nom_hu: objResult.nom_hu,
            nom_via: objResult.nom_via,
            numero: objResult.numero,
            provincia: objResult.provincia,
            tipo_via: objResult.tipo_via,
            ubigeo: objResult.ubigeo,
            x: objResult.x,
            y: objResult.y,
            zonas: objResult.zonas
        };

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "${saveGeoFinderSearchURL}",
            data: JSON.stringify(dataString),
            async: false,
            cache: false,
            processData:false,
            success: function(dataResponse){
                if(dataResponse != null){
                    console.log('dataResponse1: '+dataResponse);
                    var objResponseSave = JSON.parse(dataResponse);
                    console.log('dataResponse: '+objResponseSave);
                    console.log('objResponseSave.addressOfUse: '+objResponseSave.addressOfUse);
                    if(objResponseSave.status == "OK"){
                        //alert("Direcci�n guardada con exito.");
                        console.log("Direcci�n guardada con exito.");
                        if(appOrigin == "<%=Constantes.C_PORTLET_EVAL_LOCATION_PARAM_INCIDENT_ORIGIN_VALUE%>"){
                            updateHdnAddress(objResponseSave.addressOfUse);
                        }
                    }else{
                        //alert("Ocurri� un error al guardar la direcci�n.");
                        console.log("Ocurri� un error al guardar la direcci�n.");
                    }

                }
                //btnGuardar.attr('disabled', false);
                $('#gf_buscar').attr("disabled", false);
                $("#gf_buscar").attr('value', 'Buscar');
            },
            beforeSend: function ( xhr ) {
                //btnGuardar.attr('disabled', true);
                $("#gf_buscar").attr('value', 'Guardando');
                $('#gf_buscar').attr("disabled", true);

            },
            error: function (request, status, error) {
                if(request.status == 500) {
                    alert(request.responseText);
                }
                //btnGuardar.attr('disabled', false);
                $('#gf_buscar').attr("disabled", false);
                $("#gf_buscar").attr('value', 'Buscar');

            }
        });

    }

    function updateHdnAddress(addressOfUse){
        parent.document.frmdatos.hdnFlagEvalCoverage.value = addressOfUse.coverages; //portega PRY-1443
        parent.document.frmdatos.hdnRegion.value = addressOfUse.departamento;
        parent.document.frmdatos.hdnProvince.value = addressOfUse.provincia;
        parent.document.frmdatos.hdnDistrict.value = addressOfUse.distrito;
        parent.document.frmdatos.hdnUbigeo.value = addressOfUse.ubigeo;
        parent.document.frmdatos.hdnHomeServiceZone.value = addressOfUse.homeServiceZone;
        <!-- BEGIN: PRY-1313 - DOLANO -->
        parent.document.frmdatos.hdnCodTypeProd.value = "<c:out value="${prmCodTypeProd}"/>";
        parent.document.frmdatos.hdnPhone.value = "<c:out value="${prmPhone}"/>";
        <!-- END: PRY-1313 - DOLANO -->
    }

    function cleanHdnAddress(){
        parent.document.frmdatos.hdnFlagEvalCoverage.value = '';
        parent.document.frmdatos.hdnRegion.value = '';
        parent.document.frmdatos.hdnProvince.value = '';
        parent.document.frmdatos.hdnDistrict.value = '';
        parent.document.frmdatos.hdnUbigeo.value = '';
        parent.document.frmdatos.hdnHomeServiceZone.value = '';
        <!-- BEGIN: PRY-1313 - DOLANO -->
        parent.document.frmdatos.hdnCodTypeProd.value = '';
        parent.document.frmdatos.hdnPhone.value = '';
        <!-- END: PRY-1313 - DOLANO -->
    }

</script>

</html>