<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="../../commons/importCommons.jsp" %> <!--LGONZALES PRY-1140-->
<html>
    <head>
        <title>GeoFinder Exception</title>
    </head>
     
    <body>
        <h1>Error en la Carga del Componente de Evaluaci&oacute;n de Cobertura...</h1>
        <h2>Excepci&oacute;n ocurrida a las: <fmt:formatDate value="${exception.date}" pattern="dd-MM-yyyy HH:mm:ss" /></h2>
        <h3>Mensaje de Error: </h3>${exception.message}
    </body>

    <script type="text/javascript">

        <c:if test="${not empty exception.addressOfUse}" >
            var addressOfUse = <c:out value='${exception.addressOfUse}'/>;
            if(addressOfUse.appOrigen == "<%=Constantes.C_PORTLET_EVAL_LOCATION_PARAM_INCIDENT_ORIGIN_VALUE%>"){
                updateHdnAddress(addressOfUse.addressOfUse);
            }
        </c:if>

        function updateHdnAddress(addressOfUse){
            parent.document.frmdatos.hdnFlagEvalCoverage.value = addressOfUse.coverages;
            parent.document.frmdatos.hdnRegion.value = addressOfUse.departamento;
            parent.document.frmdatos.hdnProvince.value = addressOfUse.provincia;
            parent.document.frmdatos.hdnDistrict.value = addressOfUse.distrito;
            parent.document.frmdatos.hdnUbigeo.value = addressOfUse.ubigeo;
            parent.document.frmdatos.hdnHomeServiceZone.value = addressOfUse.homeServiceZone;
        }
    </script>
</html>