<%@page contentType="text/html; charset=iso-8859-1" pageEncoding="iso-8859-1"%>
<%@ include file="../commons/importCommons.jsp" %>
<%! private Logger log = Logger.getLogger(this.getClass()); %>
<% log.info("*****Empezamos la pagina de JP_GEOFINDER_FOR_INCIDENT_show_page.jsp******"); %>

<spring:url value="/resources/css/salesweb.css"  var="salesWebCSS"/>
<spring:url value="/resources/js/library.js"  var="libraryJS"/>
<spring:url value="/resources/js/jQuery-min.js"  var="jqueryMinJS"/>
<spring:url value="/resources/js/util.js"  var="utilJS"/>
<!--LGONZALES PRY-1140-->
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />

<link rel="stylesheet" href="${salesWebCSS}">
<script type="text/javascript" src="${libraryJS}"></script>
<script type="text/javascript" src="${jqueryMinJS}"></script>
<script type="text/javascript" src="${utilJS}"></script>

<script type="text/javascript">
$(document).ready(function(){
});
function sendAddressToIncident(){
    var flagCoverages = document.frmdatos.hdnFlagEvalCoverage.value;
    var newReg = document.frmdatos.hdnRegion.value;
    var newProv = document.frmdatos.hdnProvince.value;
    var newDist = document.frmdatos.hdnDistrict.value;
    var newUbigeo = document.frmdatos.hdnUbigeo.value;
    var newHsz = document.frmdatos.hdnHomeServiceZone.value;
    // BEGIN: PRY-1313 - DOLANO
    var codTypeProd = document.frmdatos.hdnCodTypeProd.value;
    var phone = document.frmdatos.hdnPhone.value;
    console.log("codTypeProd: " + codTypeProd + ", phone: " + phone);
    // END: PRY-1313 - DOLANO

    console.log(flagCoverages, newReg, newProv, newDist, newHsz, newUbigeo);

    // BEGIN: PRY-1313 - DOLANO
    //if(flagCoverage == 1){
    console.log("codTypeProd: " + codTypeProd + ", flagCoverages: " + flagCoverages);
    var resultVal = validateRegionChange(codTypeProd, flagCoverages);
    var result = resultVal[0];
    var message = resultVal[1];
    var resolution = resultVal[2];
    console.log("result: " + result + ", message: " + message + ", resolution: " + resolution);

    // Si la cobertura destino permite cambio de region bafi
    if(result){
        try {
            window.opener.loadDataFromGeofinder(newReg, newProv, newDist, newHsz, newUbigeo, flagCoverages, result, resolution, phone);
        } catch (err) {
            console.log(err.description || err);
        }
        window.close();
    }
    // Si la cobertura destino no permite cambio de region bafi
    else {
        //if(!confirm("La zona seleccionada no le permitir� realizar un Cambio de Region.\nPresione Aceptar para elegir otra zona.\nPresione Cancelar para cerrar la ventana.")){
        // Si selecciona Cancelar se manda los valores al PLSQL y se cierra el popup del mapa de Geofinder
        if(!confirm(message)){
            try {
                window.opener.loadDataFromGeofinder(newReg, newProv, newDist, newHsz, newUbigeo, flagCoverages, result, resolution, phone);
            } catch (err) {
                console.log(err.description || err);
            }
            window.close();
        }
    }
    // END: PRY-1313 - DOLANO
}
    // BEGIN: PRY-1313 - DOLANO
    /* Valida si se permite el cambio de region segun el tipo de producto BAFI y el resultado del flag de cobertura.
        codTypeProd: 1=NO BAFI2300, 2=BAFI2300 INDOOR, 3=BAFI2300 OUTDOOR
        flagCoverage: 2: BAFI2300 INDOOR/OUTDOOR, 3: BAFI2300 OUTDOOR, 4: BAFI2300 INDOOR
    */
var CODTYPE_2300_INDOOR="2";
var CODTYPE_2300_OUTDOOR="3";
var COB_NO_BAFI="0";
var COB_2300_IO="2"; //2300 INDOOR OUTDOOR, no se encuentra en la tabla
var COB_2300_INDOOR="3";
var COB_2300_OUTDOOR="4";
var COB_FALLA_TECNICA="-1";
var COB_BUSQUEDA_INVALIDA="-2";
var COB_3500_INDOOR="6";
var COB_3500_OUTDOOR="7";

    function validateRegionChange(codTypeProd, flagCoverages){

        var result = false; // Solo si es TRUE permitira cambio de region
        var msgFinal = ""; // Mensaje de validacion en caso no permita cambio de region
        var resolution = "1"; // 1=Cambio Regio BAFI, 2=Sin cobertura/Falla tecnica, 3=No cumple con cobertura 2300

        // Labels tipos de productos
        var prodBafi2300Indoor = "BAFI 2300 Indoor"; // codTypeProduct=2
        var prodBafi2300Outdoor = "BAFI 2300 Outdoor"; // codTypeProduct=3

        // Labels coberturas destino
        var covNoBafi2300 = "sin Cobertura 2300"; // flagCoverage=0destino
        var covNoBafi = "sin Cobertura"; // flagCoverage=0
        var covBafi2300IndOut = "con Cobertura 2300 Indoor/Outdoor"; // flagCoverage=2
        var covBafi2300Outdoor = "con Cobertura 2300 Outdoor"; // flagCoverage=3
        var covBafi2300Indoor = "con Cobertura 2300 Indoor"; // flagCoverage=4

        // Labels mensajes
        var msgNoChange = "No se puede realizar un cambio de Regi�n de un producto @param1 a una zona @param2.";
        var msgInvalidSearch = "La zona seleccionada no le permitir� realizar un Cambio de Region.";
        var msgTechnicalFailure = "Ocurri� una falla t�cnica con el componente.";
        var msgAdd = "\nPresione Aceptar para elegir otra zona.\nPresione Cancelar para cerrar la ventana.";


        if(flagCoverages==null || flagCoverages===""){
            if(codTypeProd === CODTYPE_2300_OUTDOOR)
                msgFinal = msgNoChange.replace("@param1", prodBafi2300Outdoor);
            else if(codTypeProd === CODTYPE_2300_INDOOR)
                msgFinal = msgNoChange.replace("@param1", prodBafi2300Indoor);

            msgFinal = msgFinal.replace("@param2", covNoBafi);
            msgFinal = msgFinal.concat(msgAdd);
            resolution = 2;
            return [result, msgFinal, resolution];
        }

        var coveragesArray=flagCoverages.split(",");

        // Falla tecnica o busqueda invalida
        if(codTypeProd === CODTYPE_2300_INDOOR || codTypeProd === CODTYPE_2300_OUTDOOR){ // portega
            if(validateIndexOf(coveragesArray, COB_FALLA_TECNICA)){
                msgFinal = msgTechnicalFailure;
                resolution = 2;
            } else if(validateIndexOf(coveragesArray, COB_BUSQUEDA_INVALIDA)){
                msgFinal = msgInvalidSearch;
                resolution = 2;
            }
        }

        // Si son otros flags de cobertura
        if(msgFinal == ""){
            if(codTypeProd == CODTYPE_2300_INDOOR){ // Indoor
                if(validateIndexOf(coveragesArray, COB_2300_IO)){ // Indoor/Outdoor
                    result = true;
                    resolution = 1;
                } else if(validateIndexOf(coveragesArray, COB_2300_OUTDOOR)){ // Outdoor
                    msgFinal = msgNoChange.replace("@param1", prodBafi2300Indoor);
                    msgFinal = msgFinal.replace("@param2", covBafi2300Outdoor);
                    resolution = 3;
                } else if(validateIndexOf(coveragesArray, COB_2300_INDOOR)){ // Indoor
                    result = true;
                    resolution = 1;
                } else if(validateIndexOf(coveragesArray, COB_NO_BAFI)){ // No BAFI2300
                    msgFinal = msgNoChange.replace("@param1", prodBafi2300Indoor);
                    msgFinal = msgFinal.replace("@param2", covNoBafi2300);
                    resolution = 2;
}
            } else if(codTypeProd == CODTYPE_2300_OUTDOOR){ // Outdoor
                if(validateIndexOf(coveragesArray, COB_2300_IO)){ // Indoor/Outdoor
                    result = true;
                    resolution = 1;
                } else if(validateIndexOf(coveragesArray, COB_2300_OUTDOOR)){ // Outdoor
                    result = true;
                    resolution = 1;
                } else if(validateIndexOf(coveragesArray, COB_2300_INDOOR)){ // Indoor
                    msgFinal = msgNoChange.replace("@param1", prodBafi2300Outdoor);
                    msgFinal = msgFinal.replace("@param2", covBafi2300Indoor);
                    resolution = 3;
                } else if(validateIndexOf(coveragesArray, COB_NO_BAFI)){ // No BAFI2300
                    msgFinal = msgNoChange.replace("@param1", prodBafi2300Outdoor);
                    msgFinal = msgFinal.replace("@param2", covNoBafi2300);
                }
            }
        }

        //portega validacion bafi 3500, solo si NO pasaron las validacion 2300
        if(!result){
            var resultArray=validateRegionChange3500(msgFinal, result, resolution, codTypeProd, coveragesArray);
            //portega si no paso validacion 3500
            if (!resultArray[0]) {
                msgFinal=resultArray[1];
                resolution=resultArray[2];
            }
        }

        if(msgFinal != ""){
            msgFinal = msgFinal.concat(msgAdd);
        }

        if(msgFinal == "" && !result){
            result = false;
            msgFinal = msgInvalidSearch;
            resolution = 2;
        }
        console.log("se retornaran los valores");
        console.log(result, msgFinal, resolution);
        return [result, msgFinal, resolution];
    }
    // END: PRY-1313 - DOLANO

    function validateIndexOf(coveragesArray, value){
        return coveragesArray.indexOf(value) >= 0;
    }

function validateRegionChange3500(msgFinal, result, resolution, codTypeProd, coveragesArray){
    // Labels tipos de productos
    var prodBafi2300Indoor = "BAFI 2300 Indoor"; // codTypeProduct=2
    var prodBafi2300Outdoor = "BAFI 2300 Outdoor"; // codTypeProduct=3
    var covBafi3500IO = "con Cobertura BAFI 3500 Indoor/Outdoor"; // flagCoverage= 6 y/o 7

    // Labels mensajes
    var msgNoChange = "No se puede realizar un cambio de Regi�n de un producto @param1 a una zona @param2.";

    //validacion
    var bafi3500CIO= (validateIndexOf(coveragesArray, COB_3500_INDOOR) // 3500 Indoor
        && validateIndexOf(coveragesArray, COB_3500_OUTDOOR)); // 3500 Outdoor

    if(msgFinal === "") {
        if(codTypeProd === CODTYPE_2300_INDOOR) { // 2300 Indoor
            switch (true) {
                case bafi3500CIO: // 3500 Indoor y Outdoor
                    msgFinal = msgNoChange.replace("@param1", prodBafi2300Indoor);
                    msgFinal = msgFinal.replace("@param2", covBafi3500IO);
                    resolution = 3;
                    break;
                case validateIndexOf(coveragesArray, COB_2300_INDOOR) && bafi3500CIO: // 2300 Indoor + 3500 Indoor/Outdoor
                case validateIndexOf(coveragesArray, COB_2300_IO) && bafi3500CIO: // 2300 Indoor/Outdoor + 3500 Indoor/Outdoor
                    result = true;
                    resolution = 1;
                    break;
            }
        }else if(codTypeProd === CODTYPE_2300_OUTDOOR) { // 2300 Outdoor
            switch (true) {
                case bafi3500CIO: // 3500 Indoor y Outdoor
                    msgFinal = msgNoChange.replace("@param1", prodBafi2300Outdoor);
                    msgFinal = msgFinal.replace("@param2", covBafi3500IO);
                    resolution = 3;
                    break;
                case validateIndexOf(coveragesArray, COB_2300_OUTDOOR) && bafi3500CIO: // 2300 Outdoor + 3500 Indoor/Outdoor
                case validateIndexOf(coveragesArray, COB_2300_IO) && bafi3500CIO: // 2300 Indoor/Outdoor + 3500 Indoor/Outdoor
                    result = true;
                    resolution = 1;
                    break;
            }
        }
    }

    return [result, msgFinal, resolution];
}

</script>

</head>
<body>
	<form id="frmdatos" name="frmdatos" method="post">
		<input type="hidden" name="hdnFlagEvalCoverage" />
        <input type="hidden" name="hdnRegion" />
        <input type="hidden" name="hdnProvince" />
        <input type="hidden" name="hdnDistrict" />
        <input type="hidden" name="hdnUbigeo" />
        <input type="hidden" name="hdnHomeServiceZone" />
        <!-- BEGIN: PRY-1313 - DOLANO -->
        <input type="hidden" name="hdnCodTypeProd" />
        <input type="hidden" name="hdnPhone" />
        <!-- END: PRY-1313 - DOLANO -->

        <table class="RegionBorder"  width="100%">
            <tr>
                <td class="CellContent" colspan="4" align="center">
                    <c:if test="${dynamicFrmGeoFinderComponent != null}">
                        <c:out value="${dynamicFrmGeoFinderComponent}" escapeXml="false"/>
                    </c:if>
                </td>
            </tr>
            <tr>
                <td class="CellContent" colspan="4" align="center"></td>
            </tr>
            <tr>
                <td class="CellContent" colspan="4" align="center">
                    <input  type="button" id="btnSave" onclick="javascript:sendAddressToIncident()" value="Grabar" />
                </td>
            </tr>
        </table>
	</form>
</body>
</html>