function verPaginaPopUp(pag, nom, x, y) {
	w = (screen.width / 2) - (x / 2);
	h = (screen.height / 2) - (y / 2);
	window.open(pag, nom, "scrollbars=1,status=false,width=" + x + ",height=" + y + ",top=" + h
			+ ",left=" + w);
}

function enviarPopUp(txForm, metodo, accion, x, y) {
	if (txForm != '') {
		var objForm = document.getElementById(txForm);
		objForm.method.value = metodo;
		objForm.action = accion;
		objForm.target = "popup";
		w = (screen.width / 2) - (x / 2);
		h = (screen.height / 2) - (y / 2);
		window.open("", "popup", "scrollbars=1,status=false,width=" + x
				+ ",height=" + y + ",top=" + h + ",left=" + w);
		objForm.submit();
	} else {
		return false;
	}
}

function validarFechaActualMenorParametro(fechaSolicitada){
	var todate = new Date();
	var fechas = fechaSolicitada.split('/');
	var newDate = new Date(fechas[2], fechas[1]-1, fechas[0]);
		if(newDate > todate ){
			//alert("Fecha registrada mayor a la fecha actual.");
			return false;
		}
	return true;
}